﻿/*
    This file is part of Mocktrix.
    Copyright (C) 2024, 2025  Dirk Stolle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System.Text.Json.Nodes;

namespace Mocktrix
{
    /// <summary>
    /// Handles mock data for test cases.
    /// </summary>
    public class MockData
    {
        /// <summary>
        /// Adds data for use in tests.
        /// </summary>
        public static void Add()
        {
            // User "alice" on homeserver at example domain.
            var alice = Database.Memory.Users.CreateUser("@alice:matrix.example.org", "secret password");
            _ = Database.Memory.Devices.CreateDevice("AliceDeviceId", alice.user_id, "Alice's Matrix-enabled comm badge");

            // User "alice" for domain name of the server.
            var base_address = new Uri("http://localhost:5289");
            var alice_local = Database.Memory.Users.CreateUser("@alice:" + base_address.Host, "Alice's secret password");
            _ = Database.Memory.Devices.CreateDevice("AliceDeviceId", alice_local.user_id, "Alice's Matrix-enabled comm badge");

            // User "bob" on homeserver at example domain.
            _ = Database.Memory.Users.CreateUser("@bob:matrix.example.org", "secret password");
            // User "bob" for domain name of the server.
            _ = Database.Memory.Users.CreateUser("@bob:" + base_address.Host, "secret password");

            // User for test of logging out all access tokens of a user at once.
            _ = Database.Memory.Users.CreateUser("@all_alice:matrix.example.org", "my secret password");

            AddProfileTestData(base_address);

            // User for password change tests.
            _ = Database.Memory.Users.CreateUser("@password_change:" + base_address.Host, "the old password");

            // Users for account deactivation tests.
            var inactive_user = Database.Memory.Users.CreateUser("@inactive:" + base_address.Host, "some password");
            inactive_user.inactive = true;

            _ = Database.Memory.Users.CreateUser("@deactivatable:" + base_address.Host, "silly password");

            // Add content for use in repository tests.
            _ = ContentRepository.Memory.Media.Create("testDownload", "Hello, test code. :)"u8.ToArray(), "text/plain", "hello.txt");

            AddRoomData(base_address);
            AddTagData(base_address);
            AddTagDataForSync(base_address);
            AddClientConfigData(base_address);
            AddSyncData(base_address);
        }

        private static void AddProfileTestData(Uri base_address)
        {
            // Users for display name testing.
            var display_name_user = Database.Memory.Users.CreateUser("@unnamed_user:" + base_address.Host, "bad password");
            display_name_user.display_name = null;

            var user_with_name = Database.Memory.Users.CreateUser("@named_user:" + base_address.Host, "some password");
            user_with_name.display_name = "Nomen Nominandum";

            var name_change_user = Database.Memory.Users.CreateUser("@name_change_user:" + base_address.Host, "some password");
            name_change_user.display_name = "No Name";

            // Users for avatar URL testing.
            var no_avatar = Database.Memory.Users.CreateUser("@no_avatar:" + base_address.Host, "bad password");
            no_avatar.avatar_url = null;

            var user_with_avatar = Database.Memory.Users.CreateUser("@avatar_user:" + base_address.Host, "some password");
            user_with_avatar.avatar_url = "mxc://matrix.org/FooBar";

            var avatar_change_user = Database.Memory.Users.CreateUser("@avatar_change_user:" + base_address.Host, "some password");
            avatar_change_user.avatar_url = "mxc://matrix.org/SomeOpaqueIdentifier";

            // User for profile testing.
            var profile_user = Database.Memory.Users.CreateUser("@profile:" + base_address.Host, "don't use this password");
            profile_user.avatar_url = "mxc://matrix.org/DifferentMediaId";
            profile_user.display_name = "Profiler";
        }

        private static void AddRoomData(Uri base_address)
        {
            // User and room memberships for test of joined rooms.
            var joined_user = Database.Memory.Users.CreateUser("@joined_user:" + base_address.Host, "the password");
            Database.Memory.RoomMemberships.Create("!first_joined_room:matrix.example.org", joined_user.user_id, Enums.Membership.Join);
            Database.Memory.RoomMemberships.Create("!second_joined_room:matrix.example.org", joined_user.user_id, Enums.Membership.Join);
            Database.Memory.RoomMemberships.Create("!banned_room:matrix.example.org", joined_user.user_id, Enums.Membership.Ban);
            Database.Memory.RoomMemberships.Create("!invited_room:matrix.example.org", joined_user.user_id, Enums.Membership.Invite);
            Database.Memory.RoomMemberships.Create("!knock_room:matrix.example.org", joined_user.user_id, Enums.Membership.Knock);
            Database.Memory.RoomMemberships.Create("!left_room:matrix.example.org", joined_user.user_id, Enums.Membership.Leave);

            _ = Database.Memory.Users.CreateUser("@not_a_joined_user:" + base_address.Host, "some password");

            // Room visibility tests (getting visibility data).
            _ = Database.Memory.Rooms.Create("!public_test_room:matrix.example.org", "@tester:matrix.example.org", "1", true);
            _ = Database.Memory.Rooms.Create("!private_test_room:matrix.example.org", "@tester:matrix.example.org", "1", false);

            // Room visibility tests (setting new visibility).
            _ = Database.Memory.Rooms.Create("!visibility_test_room_pub:matrix.example.org", "@alice:matrix.example.org", "1", true);
            _ = Database.Memory.Rooms.Create("!visibility_test_room_priv:matrix.example.org", "@alice:matrix.example.org", "1", false);
            _ = Database.Memory.Rooms.Create("!visibility_test_room_bob:matrix.example.org", "@bob:matrix.example.org", "1", false);
            _ = Database.Memory.Rooms.Create("!visibility_test_room_no_change:matrix.example.org", "@alice:matrix.example.org", "1", false);

            // Room alias tests.
            var alias_test_room = Database.Memory.Rooms.Create("!alias_test_room:matrix.example.org", "@alice:matrix.example.org", "1", false);
            _ = Database.Memory.RoomAliases.Create(alias_test_room.RoomId, "#test_alias_one:matrix.example.org", alias_test_room.Creator);
            _ = Database.Memory.RoomAliases.Create(alias_test_room.RoomId, "#test_alias_two:matrix.example.org", alias_test_room.Creator);
            _ = Database.Memory.RoomMemberships.Create(alias_test_room.RoomId, alias_test_room.Creator, Enums.Membership.Join);

            var world_readable_alias_test_room = Database.Memory.Rooms.Create("!world_readable_alias_test_room:matrix.example.org", "@alice:matrix.example.org", "1", false);
            _ = Database.Memory.RoomMemberships.Create(world_readable_alias_test_room.RoomId, world_readable_alias_test_room.Creator, Enums.Membership.Join);
            _ = Database.Memory.RoomAliases.Create(world_readable_alias_test_room.RoomId, "#world_readable_alias_one:matrix.example.org", world_readable_alias_test_room.Creator);
            _ = Database.Memory.RoomAliases.Create(world_readable_alias_test_room.RoomId, "#world_readable_alias_two:matrix.example.org", world_readable_alias_test_room.Creator);
            world_readable_alias_test_room.HistoryVisibility = Enums.HistoryVisibility.WorldReadable;
        }

        private static void AddTagData(Uri base_address)
        {
            // User and rooms and tags for test of tagged rooms.
            string tag_user_id = "@tag_user:" + base_address.Host;
            var tag_user = Database.Memory.Users.CreateUser(tag_user_id, "secret password");

            const string room_without_tags_id = "!room_without_tags:matrix.example.org";
            _ = Database.Memory.Rooms.Create(room_without_tags_id, tag_user_id, "1", false);
            Database.Memory.RoomMemberships.Create(room_without_tags_id, tag_user.user_id, Enums.Membership.Join);

            const string room_with_some_tags_id = "!room_with_some_tags:matrix.example.org";
            _ = Database.Memory.Rooms.Create(room_with_some_tags_id, tag_user_id, "1", false);
            Database.Memory.RoomMemberships.Create(room_with_some_tags_id, tag_user.user_id, Enums.Membership.Join);

            Database.Memory.Tags.Create(tag_user_id, room_with_some_tags_id, "m.favourite", 0.25);
            Database.Memory.Tags.Create(tag_user_id, room_with_some_tags_id, "u.null", null);
            Database.Memory.Tags.Create(tag_user_id, room_with_some_tags_id, "u.some_tag", 1.0);

            // Data for tag deletion tests.
            const string room_with_tag_to_delete_id = "!room_with_tag_to_delete:matrix.example.org";
            _ = Database.Memory.Rooms.Create(room_with_tag_to_delete_id, tag_user_id, "1", false);
            Database.Memory.RoomMemberships.Create(room_with_tag_to_delete_id, tag_user.user_id, Enums.Membership.Join);

            Database.Memory.Tags.Create(tag_user_id, room_with_tag_to_delete_id, "u.delete_me", 0.5);
            Database.Memory.Tags.Create(tag_user_id, room_with_tag_to_delete_id, "u.keep_me", 0.25);

            // Data for room where tags are added.
            const string room_to_add_tag_to_id = "!room_to_add_tag_to:matrix.example.org";
            _ = Database.Memory.Rooms.Create(room_to_add_tag_to_id, tag_user_id, "1", false);
            Database.Memory.RoomMemberships.Create(room_to_add_tag_to_id, tag_user.user_id, Enums.Membership.Join);

            const string room_to_change_tag_id = "!room_to_change_tag:matrix.example.org";
            _ = Database.Memory.Rooms.Create(room_to_change_tag_id, tag_user_id, "1", false);
            Database.Memory.RoomMemberships.Create(room_to_change_tag_id, tag_user.user_id, Enums.Membership.Join);
            Database.Memory.Tags.Create(tag_user_id, room_to_change_tag_id, "u.existing", 0.5);
        }

        private static void AddTagDataForSync(Uri base_address)
        {
            // User and rooms and tags for test of sync with tags.
            string tag_user_id = "@sync_tag_user:" + base_address.Host;
            var tag_user = Database.Memory.Users.CreateUser(tag_user_id, "secret password");

            {
                const string joined_room_with_tags_id = "!joined_room_with_some_tags:matrix.example.org";
                _ = Database.Memory.Rooms.Create(joined_room_with_tags_id, tag_user_id, "1", false);
                Database.Memory.RoomMemberships.Create(joined_room_with_tags_id, tag_user.user_id, Enums.Membership.Join);

                Database.Memory.Tags.Create(tag_user_id, joined_room_with_tags_id, "m.favourite", 0.25);
                Database.Memory.Tags.Create(tag_user_id, joined_room_with_tags_id, "u.null", null);
                Database.Memory.Tags.Create(tag_user_id, joined_room_with_tags_id, "u.some_tag", 1.0);
            }

            {
                const string left_room_with_tags_id = "!left_room_with_some_tags:matrix.example.org";
                _ = Database.Memory.Rooms.Create(left_room_with_tags_id, tag_user_id, "1", false);
                Database.Memory.RoomMemberships.Create(left_room_with_tags_id, tag_user.user_id, Enums.Membership.Leave);

                Database.Memory.Tags.Create(tag_user_id, left_room_with_tags_id, "u.ooooh", 0.25);
                Database.Memory.Tags.Create(tag_user_id, left_room_with_tags_id, "u.null", null);
                Database.Memory.Tags.Create(tag_user_id, left_room_with_tags_id, "u.some_tag", 0.75);
            }
        }


        private static void AddClientConfigData(Uri base_address)
        {
            string user_id = "@account_data_user:" + base_address.Host;
            var account_data_user = Database.Memory.Users.CreateUser(user_id, "secret password");

            {
                JsonNode? node = JsonNode.Parse("""
                {
                    "snow": "glistening",
                    "sleigh_bells": "ring, ring",
                    "building_snowman": true
                }
                """);
                _ = Database.Memory.ConfigData.Create(user_id, "x.mas.song", node!);
            }

            {
                JsonNode? node = JsonNode.Parse("""
                {
                    "snow": "glistening",
                    "sleigh_bells": "ring, ring, ring",
                    "building_snowman": true
                }
                """);
                const string room_id = "!account_data_room:matrix.example.org";
                _ = Database.Memory.Rooms.Create(room_id, user_id, "1", false);
                _ = Database.Memory.RoomMemberships.Create(room_id, account_data_user.user_id, Enums.Membership.Join);
                _ = Database.Memory.RoomConfigData.Create(user_id, room_id, "org.snow.data", node!);
            }
        }

        private static void AddSyncData(Uri base_address)
        {
            string sync_user_id = "@sync_user_with_account_data:" + base_address.Host;
            _ = Database.Memory.Users.CreateUser(sync_user_id, "secret password");

            // user-specific config data
            {
                JsonNode? node = JsonNode.Parse("""
                {
                    "foo": "bar",
                    "baz": "quux"
                }
                """);
                _ = Database.Memory.ConfigData.Create(sync_user_id, "org.example.foo", node!);
            }

            {
                JsonNode? node = JsonNode.Parse("""
                {
                    "hey": "there",
                    "go": true
                }
                """);
                _ = Database.Memory.ConfigData.Create(sync_user_id, "org.test.hey", node!);
            }

            // room-specific config data
            {
                const string joined_room_id = "!joined_room_with_account_data:matrix.example.org";
                _ = Database.Memory.Rooms.Create(joined_room_id, sync_user_id, "1", false);
                _ = Database.Memory.RoomMemberships.Create(joined_room_id, sync_user_id, Enums.Membership.Join);
                {
                    JsonNode? node = JsonNode.Parse("""
                    {
                        "what": "joined room config data",
                        "count": 3
                    }
                    """);
                    _ = Database.Memory.RoomConfigData.Create(sync_user_id, joined_room_id, "test.join.data", node!);
                }
            }

            {
                const string left_room_id = "!left_room_with_account_data:matrix.example.org";
                _ = Database.Memory.Rooms.Create(left_room_id, sync_user_id, "1", false);
                _ = Database.Memory.RoomMemberships.Create(left_room_id, sync_user_id, Enums.Membership.Leave);
                {
                    JsonNode? node = JsonNode.Parse("""
                    {
                        "what": "left room config data",
                        "count": 5
                    }
                    """);
                    _ = Database.Memory.RoomConfigData.Create(sync_user_id, left_room_id, "test.leave.data", node!);
                }
            }
        }
    }
}
